const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const UserModel = require('../models/user');

const cookie_name = process.env.COOKIE_NAME
const jwt_secret = process.env.JWT_SECRET
const expire_time = +process.env.EXPIRE_TIME

router.post('/login', function (req, res) {
    passport.authenticate('local', {session: false}, (err, user, info) => {
        console.log("asd", err)
        console.log("asd1", user)
        if (err || !user) {
            return res.json({
                success: false,
                user: user
            });
        }
        req.login(user, {session: false}, (err) => {
            if (err) {
                res.json({success: false, error: err.message});
            }
            const options = {
                username: user.username,
                id: user.id,
                email: user.email
            }

            const token = jwt.sign(options, jwt_secret, {
                expiresIn: expire_time
            });
            res.cookie(cookie_name, token, {expires: new Date(Date.now() + expire_time * 1000), httpOnly: true})
            res.json({success: true})
        });
    })(req, res);
});

router.get('/logout', (req, res) => {
    res.clearCookie(cookie_name)
    res.redirect('/')
});

router.post('/reg', function (req, res) {
    if (req.body.password !== req.body.confirm) {
        return res.json({success: false, error: "Wrong confirmation password"})
    }
    UserModel.create({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    }, (err, success) => {
        if (err) return res.json({success: false, error: err.message})
        res.json({success: true})
    })
});

module.exports = router