const express = require('express');
const router = express.Router();
const cookie_name = process.env.COOKIE_NAME
const UserModel = require('../models/user.js')
const jwt = require('jsonwebtoken')

router.get('/client_id', (req, res) => {
    res.json({
        success: true,
        client_id: process.env.CLIENT_ID,
        redirect_url: process.env.REDIRECT_URL
    })
})

router.post('/token', (req, res) => {
    console.log("TOKEN")
    if (process.env.CLIENT_ID !== req.body.client_id) return loginFail(res)

    const token = req.body.jwt
    try {
        let {id, username, email} = jwt.verify(token, process.env.JWT_SECRET)
        console.log({id, username})
        UserModel.findOne({sso_id: id, username: username})
            .then((user) => {
                !user && UserModel.create({sso_id: id, username: username, email}, (err, _user) => {
                    if (err) return loginFail(res, err)
                })
                res.cookie(cookie_name, token)
                return res.redirect('/')
            })
            .catch((err) => {
                return loginFail(res, err)
            })
    } catch (err) {
        return loginFail(res, err)
    }
})

function loginFail(res, err) {
    if (err) console.log(err)
    res.cookie(cookie_name, 'fail')
    res.redirect('/')
}


module.exports = router


