const express = require('express');
const router = express.Router();
const SpeakerModel = require('../models/speaker')

router.get('/user', (req, res) => {
    console.log(req.user)
    res.json({
            username: req.user.username,
            id: req.user.id
        }
    )
})

router.get('/speaker/:id', (req, res) => {
    SpeakerModel.findById(req.params.id)
        .then((speaker) => {
            res.json(speaker)
        })
        .catch((err) => {
            res.status(500).end(err.message)
        })
})

router.get('/speakers', (req, res) => {
    SpeakerModel.find({})
        .then((speakers) => {
            let speakersMap = {}
            speakers.forEach(function (speaker) {
                speakersMap[speaker._id] = speaker
            });
            res.json(speakersMap)
        })
        .catch((err) => {
            res.status(500).end(err.message)
        })
})

router.post('/speaker', (req, res) => {
    const owner_id = req.user.id
    SpeakerModel.create(Object.assign({owner_id}, req.body))
        .then((speaker) => {
            res.json(speaker)
        })
        .catch((err) => {
            res.status(500).end(err.message)
        })
})

router.put('/speaker/:id', (req, res) => {
    const owner_id = req.user.id
    SpeakerModel.findById(req.params.id)
        .then((speaker) => {
            if (speaker && speaker.owner_id === owner_id) {
                SpeakerModel.update({_id: req.params.id}, {$set: req.body})
                    .catch((err) => {
                        return res.status(500).end(err.message)
                    })
            } else {
                return res.status(400).end("Wrong user or speaker id.")
            }
        })
        .catch((err) => {
            return res.status(500).end(err.message)
        })
    SpeakerModel.create(Object.assign({owner_id}, req.body))
        .then((speaker) => {
            res.json(speaker)
        })
        .catch((err) => {
            res.status(500).end(err.message)
        })
})


module.exports = router