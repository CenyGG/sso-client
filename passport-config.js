const passport = require("passport");
const LocalStrategy = require('passport-local').Strategy
const passportJWT = require("passport-jwt")
const JWTStrategy = passportJWT.Strategy
const UserModel = require('./models/user')
const cookie_name = process.env.COOKIE_NAME
const jwt = require('jsonwebtoken')



const JwtFromCookiesExtractor = function (cookie) {
    return function (req) {
        console.log("123412r412rf12rf123f12f")
        console.log(cookie)
        console.log(req.cookies[cookie])
        return req.cookies[cookie]
    }
}

passport.use(new JWTStrategy({
        jwtFromRequest: JwtFromCookiesExtractor(cookie_name),
        secretOrKey: process.env.JWT_SECRET
    },
    function (jwtPayload, done) {
        const expirationDate = new Date(jwtPayload.exp * 1000)
        console.log("Hyeta s expire", expirationDate < new Date())
        if (expirationDate < new Date()) {
            return done(null, false);
        }
        done(null, jwtPayload)
    }))


passport.use(new LocalStrategy(
    function (username, password, cb) {
        return UserModel.findOne({username})
            .then(user => {
                console.log(1, user)
                if (!user) {
                    return cb(null, false, {message: 'Incorrect username.'});
                }
                user.verifyPassword(password, (err, res) => {
                    console.log(2, password)
                    console.log(3, err)
                    console.log(4, res)

                    if (err) return cb(err)
                    if (res) return cb(null, user, {message: 'Logged In Successfully'})
                    else return cb(null, false, {message: 'Incorrect password.'})
                })
            })
            .catch(cb);
    }
));