const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Article = new Schema({
    owner_id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        unique: false,
        required: true
    },
    type: {
        type: String,
        unique: false,
        required: true
    },
    short: {
        type: String,
        unique: false,
        required: false
    },
    body: {
        type: String,
        unique: false,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});


const ArticleModel = mongoose.model('Article', Article);

module.exports = ArticleModel