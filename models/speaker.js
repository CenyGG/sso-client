const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Speaker = new Schema({
    owner_id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        unique: false,
        required: true
    },
    second_name: {
        type: String,
        unique: false,
        required: true
    },
    third_name: {
        type: String,
        unique: false,
        required: false
    },
    theme: {
        type: String,
        unique: false,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    approved: {
        type: Boolean,
        default: false
    },
    dateApproved: {
        type: Date,
        required: false
    },
    approvedToPublications: {
        type: Boolean,
        default: false
    }
});


const SpeakerModel = mongoose.model('Speaker', Speaker);

module.exports = SpeakerModel