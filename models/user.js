const bcrypt = require('bcrypt-nodejs')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const User = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: false
    },
    sso_id: {
        type: String,
        unique: false,     //actual true, but it have some issues and lazy to do it now
        required: false
    }
});

User.pre('save', function (next) {
    if (!(this.password || this.sso_id)) throw Error("Nor password or sso_id specified.")
    if (this.password) {
        bcrypt.hash(this.password, null, null, (err, hash)=>{
            if(err) return next(err)
            this.password = hash
            next()
        })
    }
    next()
})

User.methods.verifyPassword = function (pass, cb) {
    bcrypt.compare(pass, this.password, (err, isMatch) => {
        if (err) return cb(err)
        cb(null, isMatch)
    })
}

const UserModel = mongoose.model('User', User);

module.exports = UserModel