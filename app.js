require('dotenv').config()
require('mongoose').connect(process.env.MONGODB_CONNECTION, () => {
    console.log("Connected to mongodb")
})
require('./passport-config')

const express = require('express')
const passport = require('passport')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')()

const loginRoute = require('./routes/login')
const apiRoute = require('./routes/api')
const publicRoute = require('./routes/public')

const port = process.env.PORT || 3000

const app = express()

app.use(passport.initialize());
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(cookieParser)

app.use('/auth', loginRoute)
app.use('/public', publicRoute)
app.use('/api', passport.authenticate('jwt', {session: false}), apiRoute)

app.get('/redirect', function (req, res) {
    console.log('redirect')
    console.log(`${process.env.REDIRECT_URL}?${process.env.CLIENT_ID}`)
    res.redirect(`${process.env.REDIRECT_URL}?${process.env.CLIENT_ID}`);
});


app.get('/*', (req, res) => {
    res.redirect('/')
})


app.listen(port, () => {
    console.log(`Listen on port ${port}`)
})