import React from 'react';

const style = {
    fontSize: 16,
    color: 'black',
}
class LoreIpsum extends React.Component {
    render() {
        return (
            <div style={style}>
              <h1 align="center">Приветствуем Вас на портале научной конференции!</h1>
                <p align="center">Здесь вы можете ознакомиться с информацией о нашей конференции, а также пройти регистрацию и стать её участником!
            <img src="https://www.sut.ru/news/data/textimages8/.thumbs/924f77d518b7a27606a5eada3a71fea4_745_0_0.jpg" style={{ width:500}} alt="СПБГУТ"></img> 
            <img src="https://www.sut.ru/news/data/textimages8/.thumbs/2b4d37e61b9d88375705ac320f3e8c68_745_0_0.jpg" style={{ width:500}} alt="СПБГУТ"></img></p>
            </div>
        );
    }
}

export default LoreIpsum