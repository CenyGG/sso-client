import React from 'react';
import { Row, Col } from 'antd';
import UserCardWrapper from './UserCardWrapper'
import { inject } from 'mobx-react';
import { observer } from "mobx-react/index";

const logoStyle = {
    fontSize: 32,
    color: 'black'
}
@inject('history')
@observer
class Header extends React.Component {

    render() {
        return (
            <Row type="flex" justify="space-around" align="middle">
                <Col/>
                <Col onClick={()=>{this.props.history.push('/')}} >
                    <img  src="https://www.ucheba.ru/pix/branding/6163/title.png" style={{ width:500}} alt="СПБГУТ" />
                </Col>
                <Col>
                    <UserCardWrapper />
                </Col>
            </Row>
        );
    }
}


export default Header