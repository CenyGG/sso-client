import { Menu, Icon, Affix, Row, Col } from 'antd';
import React from 'react';
import { inject } from 'mobx-react';
import { observer } from "mobx-react/index";
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

const menuStyle = {
    fontSize: 16
};

const wraperStyle = {
    backgroundColor: 'rgba(255, 255, 255, 0.97)',
    borderBottom: '1px solid #e8e8e8'
};

@inject('history')
@observer
class MyMenu extends React.Component {
    state = {
        current: 'mail',
    }
    handleClick = (e) => {
        console.log('click ', e);
        this.setState({
            current: e.key,
        });
        if (e.key) {
            console.log(this.props)
            this.props.history.push(`/${e.key}`)
        }

    }
    render() {
        console.log(this.props)
        return (
            <Affix>
                <Row style={wraperStyle} type="flex" justify="space-around" align="middle">
                    <Col>
                        <Menu
                            onClick={this.handleClick}
                            selectedKeys={[this.state.current]}
                            mode="horizontal"
                            style={menuStyle}
                        >
                          
                            <Menu.Item key="about">
                                О Конференции
                            </Menu.Item>
                            <Menu.Item key="program">
                                Программа
                            </Menu.Item>
                            <SubMenu title={<span><Icon type="menu-unfold" />Регистрация участия</span>}>
                               
                                    <Menu.Item key="register">Регистрация</Menu.Item>                                                             
                                    <Menu.Item key="speakers">Список участников</Menu.Item>
                             
                            </SubMenu>
                        </Menu>
                    </Col>
                </Row>
            </Affix>

        );
    }
}

export default MyMenu