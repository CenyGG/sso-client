import {Form, Input, Button, Row, Col, notification} from 'antd';
import {inject} from 'mobx-react';
import React from 'react';
import {observer} from "mobx-react/index";

const FormItem = Form.Item;

@inject('user')
@observer
class RegistrationForm extends React.Component {
    state = {
        confirmDirty: false,
        autoCompleteResult: [],
    };

    render() {
        console.log(this.props)
        const user = this.props.user;
        if (user.username) this.props.history.push("/")

        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 8},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 8},
                sm: {span: 8},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 8,
                    offset: 8,
                },
                sm: {
                    span: 8,
                    offset: 8,
                },
            },
        };

        return (
            <Row style={{height: "100vh"}} type="flex" justify="center" align="middle">
                <Col span={12}>
                    <Form onSubmit={this.handleSubmit}>
                        <FormItem
                            {...formItemLayout}
                            label={(<span>Username&nbsp;</span>)}
                        >
                            {getFieldDecorator('username', {
                                rules: [{required: true, message: 'Please input your nickname!', whitespace: true}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="E-mail"
                        >
                            {getFieldDecorator('email', {
                                rules: [{
                                    type: 'email', message: 'The input is not valid E-mail!',
                                }, {
                                    required: true, message: 'Please input your E-mail!',
                                }],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="Password"
                        >
                            {getFieldDecorator('password', {
                                rules: [{
                                    required: true, message: 'Please input your password!',
                                }, {
                                    validator: this.validateToNextPassword,
                                }],
                            })(
                                <Input type="password"/>
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="Confirm Password"
                        >
                            {getFieldDecorator('confirm', {
                                rules: [{
                                    required: true, message: 'Please confirm your password!',
                                }, {
                                    validator: this.compareToFirstPassword,
                                }],
                            })(
                                <Input type="password" onBlur={this.handleConfirmBlur}/>
                            )}
                        </FormItem>
                        <FormItem {...tailFormItemLayout}>
                            <Row type="flex" justify="center" align="middle">
                                <Col>
                                    <Button type="primary" htmlType="submit">Register</Button>
                                </Col>
                            </Row>
                        </FormItem>
                    </Form>
                </Col>
            </Row>
        );
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                fetch(`/auth/reg`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    'credentials': 'include',
                    body: JSON.stringify({
                        username: values.username,
                        email: values.email,
                        password: values.password,
                        confirm: values.confirm,
                    })
                })
                    .then((res) => {
                        if (res.status !== 200) throw new Error("Server error")
                        return res.json()
                    })
                    .then((res) => {
                        if (res.success) {
                            this.openNotificationWithIcon('success', 'User successfully registered.')
                            setTimeout(() => {
                                this.props.history.push("/")
                            }, 1500)
                        }
                        else
                            this.openNotificationWithIcon('error', res.error)
                    })
                    .catch((err) => {
                        this.openNotificationWithIcon('error', err.message)
                    })
            }
        });
    }


    openNotificationWithIcon(type, descr) {
        notification[type]({
            message: type.toUpperCase(),
            description: descr,
        });
    }


    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    }
    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    }
    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], {force: true});
        }
        callback();
    }
}

const WrappedRegistrationForm = Form.create()(RegistrationForm);
export default WrappedRegistrationForm