import React from 'react';
import { observer, inject } from "mobx-react";
import { Row, Col, Input, Button } from 'antd';
import UserCard from './UserCard'

const Search = Input.Search;



@inject('user')
@observer
class UserCardWrapper extends React.Component {
    constructor(props) {
        super(props);
        this.checkAuthorized()
    }

    render() {
        return (
            <Row type="flex" justify="center" align="middle">
                <Col>
                    {!this.props.user.username ? (
                        <div>
                            <a style={{marginRight:'1em'}} href='/login'> Sing in </a>
                            <Button href='/reg' type="primary"  ghost>Sing up</Button>
                        </div>
                    ) : (
                            <div>
                                <UserCard />
                            </div>
                        )}
                </Col>
            </Row>
        );
    }

    checkAuthorized() {
        console.log("checkAuthorized")
        fetch(`/api/user`, {
            method: 'GET',
            credentials: 'include',
        })
            .then((res) => {
                if (res.status !== 200) throw Error("Not authorized")
                return res.json()
            })
            .then((res) => {
                console.log(res)
                if (res.error) return
                this.props.user.username = res.username
                this.props.user.email = res.email
                this.props.user.id = res.id
            }).catch((err) => {
                console.log(err.message)
            })
    }
}

function SearchPanel(params) {
    return <Search
        placeholder="Search"
        onSearch={value => console.log(value)}
        style={{ width: 100, marginLeft: 20, marginRight: 20 }}
    />
}

export default UserCardWrapper