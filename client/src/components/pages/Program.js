import React, { Component } from 'react';
import Layout from '../Layout';

import { Row, Col } from 'antd';

class Program extends Component {
  render() {
    return (
      <Layout>
        <Row type="flex" justify="space-around" align="middle">
          <Col>
           
              <h2>Программа конференции</h2>
          
            <table cellPadding="5">
              <tbody>
                <tr>
                  <td> <font size="4">10:00 - 10:30</font> </td>
                  <td><h3>Регистрация участников</h3></td>
                </tr>
                <tr>
                  <td><font size="4">10:30 - 11:00</font></td>
                  <td>
                    <h3>Приветственная речь</h3>
                    <b> Кривоносова Наталья Викторовна</b> <br />
                    Преподаватель первой квалификационной категории.
                <ul>Преподаваемые дисциплины:
                  <li>Безопасность функционирования информационных систем;</li>
                      <li>Операционные системы;</li>
                      <li>Технология применения комплексной системы защиты информации;</li>
                      <li>Технология применения программно-аппаратных средств защиты информации в МТС и сетях электросвязи;</li>
                      <li>Технология разработки программного обеспечения.</li>
                    </ul>
                    <b>Гвоздев Юрий Васильевич</b> <br />
                    Проректор по безопасности.<br /> <br />
                    <b>Бачевский Сергей Викторович</b> <br />
                    Ректор.
              </td>
                </tr>
                <tr>
                  <td><font size="4">11:00 - 13:00</font></td>
                  <td><h3>Ключевые доклады</h3></td>
                </tr>
                <tr>
                  <td align="right"><font size="">11:00 - 11:30</font></td>
                  <td><b>Актуальные вопросы информационной безопасности в Интернете</b>
                    <br /> Сахаров Анатолий Викторович <br /> Специалист по защите информации.</td>
                </tr>
                <tr>
                  <td align="right"><font size="">11:30 - 12:00</font></td>
                  <td><b>Кибертерроризм как глобальная угроза информационному сообществу</b>
                    <br /> Макаров Сергей Юрьевич <br /> Доцент кафедры проектирования и безопасности компьютерных систем.</td>
                </tr>
                <tr>
                  <td align="right"><font size="">12:00 - 12:35</font></td>
                  <td><b>Информационная безопасность граждан: новые реалии и угрозы</b>
                    <br /> Бишков Василий Игоревич <br /> Начальник отдела обеспечения безопасности информационных систем.</td>
                </tr>
                <tr>
                  <td align="right"><font size="">12:35 - 13:00</font></td>
                  <td><b>Информационная безопасность в Санкт-Петербурге и Ленинградской области</b>
                    <br /> Наумова Наталья Владимировна <br /> Заведующий кафедрой защиты информации от утечки по техническим каналам.</td>
                </tr>
                <tr>
                  <td><font size="4">13:00 - 13:30</font></td>
                  <td><h3>Перерыв на обед и обсуждение докладов</h3></td>
                </tr>
                <tr>
                  <td><font size="4">13:30 - 14:30</font></td>
                  <td><h3>Дискуссия и ответы на вопросы</h3></td>
                </tr>
                <tr>
                  <td><font size="4">14:30 - 15:00</font></td>
                  <td><h3>Прощальная речь и благодарности</h3></td>
                </tr>
              </tbody>
            </table>
          </Col>
        </Row>
      </Layout>
    );
  }
}

export default Program;
