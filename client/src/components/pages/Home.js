import React, { Component } from 'react';
import LoreIpsum from "../LoreIpsum";
import Layout from '../Layout';

class Home extends Component {
    render() {
        return (
            <Layout>
                <LoreIpsum />
            </Layout>
        );
    }
}

export default Home;
