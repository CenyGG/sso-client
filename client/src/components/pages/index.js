import About from './About'
import Home from './Home'
import Program from './Program'
import RegSpeaker from './RegSpeaker'
import Speakers from './Speakers'

export { About, Home, Program, RegSpeaker, Speakers }