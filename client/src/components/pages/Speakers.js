import React, {Component} from 'react';
import {Table} from 'antd';
import Layout from '../Layout';
import {observable} from 'mobx';
import {observer} from "mobx-react/index";

const columns = [{
    title: 'Имя',
    dataIndex: 'name',
    key: 'name',
}, {
    title: 'Фамилия',
    dataIndex: 'second_name',
    key: 'second_name',
}, {
    title: 'Отчество',
    dataIndex: 'third_name',
    key: 'third_name',
}, {
    title: 'Тема',
    dataIndex: 'theme',
    key: 'theme',
}];

@observer
class Speakers extends Component {
    dataSource = observable([]);

    constructor(props){
        super(props)
        this.fetchData()
    }

    render() {
        return (
            <Layout>
                <Table dataSource={this.dataSource.slice()} columns={columns}/>
            </Layout>
        );
    }

    fetchData() {
        fetch(`/api/speakers`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            'credentials': 'include'
        })
            .then((res) => {
                if (res.status !== 200) throw new Error("Server error")
                return res.json()
            })
            .then((res) => {
                console.log(res)
                for(let id in res){
                    const speaker = res[id]
                    this.dataSource.push({
                        key: id,
                        name: speaker.name,
                        second_name: speaker.second_name,
                        third_name: speaker.third_name,
                        theme: speaker.theme
                    })
                }
            })
            .catch((err) => {
                console.log(err.message)
            })
    }
}

export default Speakers;

