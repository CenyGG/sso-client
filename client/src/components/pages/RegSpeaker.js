import React, { Component } from 'react';
import Layout from '../Layout';
import {Form, Input, Button, Row, Col, notification} from 'antd';
const FormItem = Form.Item;

class RegSpeaker extends Component {
    render() {
        const {getFieldDecorator} = this.props.form;

        return (
            <Layout>
                <Row type="flex" justify="space-around" align="middle">
                    <Col>
                        <Form onSubmit={this.handleSubmit}>
                            <FormItem
                                label={(<span>Имя&nbsp;</span>)}
                            >
                                {getFieldDecorator('name', {
                                    rules: [{required: true, message: 'Please input your name!', whitespace: true}],
                                })(
                                    <Input/>
                                )}
                            </FormItem>
                            <FormItem
                                label="Фамилия"
                            >
                                {getFieldDecorator('second_name', {
                                    rules: [{
                                        required: true, message: 'Please input your second name!',
                                    }],
                                })(
                                    <Input/>
                                )}
                            </FormItem>
                            <FormItem
                                label="Отчество"
                            >
                                {getFieldDecorator('third_name', {
                                    rules: [{
                                        required: true, message: 'Please input your second name!',
                                    }],
                                })(
                                    <Input/>
                                )}
                            </FormItem>
                            <FormItem
                                label="Тема доклада"
                            >
                                {getFieldDecorator('theme', {
                                    rules: [{
                                        required: true, message: 'Please input your theme!',
                                    }],
                                })(
                                    <Input/>
                                )}
                            </FormItem>
                            <FormItem>
                                <Row type="flex" justify="center" align="middle">
                                    <Col>
                                        <Button type="primary" htmlType="submit">Register</Button>
                                    </Col>
                                </Row>
                            </FormItem>
                        </Form>
                    </Col>
                </Row>
            </Layout>
        );
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                fetch(`/api/speaker`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    'credentials': 'include',
                    body: JSON.stringify({
                        name: values.name,
                        second_name: values.second_name,
                        third_name: values.third_name,
                        theme: values.theme
                    })
                })
                    .then((res) => {
                        if (res.status !== 200) throw new Error("Server error")
                        return res.json()
                    })
                    .then((res) => {
                        if (res.status!==500 || res.status!==400) {
                            this.openNotificationWithIcon('success', 'Speaker successfully registered.')
                            setTimeout(() => {
                                this.props.history.push("/")
                            }, 1500)
                        }
                        else
                            this.openNotificationWithIcon('error', res.error)
                    })
                    .catch((err) => {
                        this.openNotificationWithIcon('error', err.message)
                    })
            }
        });
    }


    openNotificationWithIcon(type, descr) {
        notification[type]({
            message: type.toUpperCase(),
            description: descr,
        });
    }
}

const WrappedRegSpeaker = Form.create()(RegSpeaker);
export default WrappedRegSpeaker
