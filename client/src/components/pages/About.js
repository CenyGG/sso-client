import React, { Component } from 'react';
import Layout from '../Layout';

class About extends Component {
    render() {
        console.log(this.props)
        return (
            <Layout>
                <div>
                      <b>Дата проведения:</b> 20 июля 2018 <br /> <b>Время проведения:</b> 10:00 - 15:00 <br />   <b>Место проведения:</b> СПБГУТ 
                     <br/><br/>   В Санкт-Петербурге 20 июля 2018 года состоится юбилейная 10-я
                         Санкт-Петербургская межрегиональная конференция «Информационная безопасность»
                         при участии университета и его филиалов. <br />
                        <br />
                         
                            <ul>
                                <h3>Программа конференции включает следующие научные направления:</h3>

                                <li>Государственная политика обеспечения информационной безопасности регионов России.</li>
                                <li>Правовые аспекты информационной безопасности.</li>
                                <li>Безопасность информационных технологий.</li>
                                <li>Современные средства защиты информации.</li>
                                <li>Информационная безопасность телекоммуникационных сетей.</li>
                                <li>Информационно-экономическая безопасность.</li>
                                <li>Информационная безопасность и импортозамещение в критических инфраструктурах.</li>
                                <li>Информационная безопасность транспортных систем.</li>
                                <li>Информационная безопасность киберфизических систем.</li>
                                <li>Подготовка и переподготовка кадров в области обеспечения информационной безопасности.</li>
                                <li>Актуальные вопросы информационной безопасности в Интернете.</li>
                                <li>Кибертерроризм как глобальная угроза информационному сообществу.</li>
                                <li>Информационная безопасность граждан: новые реалии и угрозы.</li>
                                <li>Информационная безопасность в Санкт-Петербурге и Ленинградской области</li>
                            </ul>
                        
                    
                      <h3>Санкт-Петербургский государственный университет телекоммуникаций им. проф. М.А.Бонч-Бруевича</h3> <br />  пр. Большевиков, 22, к. 1, Санкт-Петербург, 193232

                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2000.7897279029096!2d30.484828416029448!3d59.902439981863004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x469631053d48213d%3A0x62d73aed8d51a38f!2z0KHQsNC90LrRgi3Qn9C10YLQtdGA0LHRg9GA0LPRgdC60LjQuSDQs9C-0YHRg9C00LDRgNGB0YLQstC10L3QvdGL0Lkg0YPQvdC40LLQtdGA0YHQuNGC0LXRgiDRgtC10LvQtdC60L7QvNC80YPQvdC40LrQsNGG0LjQuSDQuNC8LiDQv9GA0L7RhC4g0JwuINCQLiDQkdC-0L3Rhy3QkdGA0YPQtdCy0LjRh9Cw!5e0!3m2!1sru!2sru!4v1526736347838" width="800" height="380"></iframe>
                    <br /><br /><br />
                </div>



            </Layout>
        );
    }
}

export default About;
