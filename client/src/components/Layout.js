import React, { Component } from 'react';
import Header from "./Header";
import Menu from "./Menu";
import { Col, Row } from "antd";


class Layout extends Component {
    render() {
        return (
            <div >
                <Row style={{ marginTop: '1em', marginBottom: '1em' }} type="flex" justify="space-around" align="middle">
                    <Col span={18}>
                        <Header />
                    </Col>
                </Row>
                <Menu />

                <Row style={{ marginTop: '2em' }} type="flex" justify="space-around" align="middle">
                    <Col span={16}>


                        {this.props.children}


                    </Col>
                </Row>
            </div>
        );
    }
}

export default Layout;
