import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {Route, Switch, Router} from 'react-router-dom';
import Login from "./components/Login";
import Register from "./components/Register";
import {Provider} from 'mobx-react';
import {observable} from 'mobx';
import './css/index.css';
import 'antd/dist/antd.css';
import * as pages from './components/pages'
import createBrowserHistory from 'history/createBrowserHistory'

const history = createBrowserHistory()
const stores = {
    @observable
    user: {
        username: null,
        email: null
    },
    @observable
    history: history
}

ReactDOM.render(
    <Provider {...stores}>
        <Router history={history}>
            <Switch>
                <Route exact path='/' component={pages.Home}/>
                <Route exact path='/login' component={Login}/>
                <Route exact path='/reg' component={Register}/>

                <Route exact path='/about' component={pages.About}/>
                <Route exact path='/home' component={pages.Home}/>    
                <Route exact path='/program' component={pages.Program}/>
                <Route exact path='/register' component={pages.RegSpeaker}/>
                <Route exact path='/speakers' component={pages.Speakers}/>
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();



